# java2021-2

#### 介绍
2021-2022-1 面向对象程序设计 源码

#### 第1周
1. 新建类（class）
2. 输入、输出
3. 例：计算圆面积

#### 第2周
1. 用Java编写结构化程序
2. 交换x,y,z
3. 判断闰年
4. 判断素数
5. 学会编写方法（函数）

#### 第3周
1. 类、对象
2. UML 类图
3. Dog 类
4. Student、Teacher类

#### 第4周
1. 变量分类（局部变量、实例变量）
2. 变量分类（主数据类型变量，引用变量）
3. 数组

#### 第6周
1. 方法
2. 方法参数传递
3. 封装
4. 构造方法/构造函数

#### 第7周
1. 继承
2. 方法覆盖(Override)

#### 第8周
1. 抽象类
2. 接口
3. 方法覆盖，方法重载
4. 继承关系中的构造方法
5. this， super

#### 第9周
1. 静态 static
2. final 
3. 静态代码块，构造代码块
4. 创建对象时，静态变量，静态代码块，实例变量，构造代码块，构造方法的执行次序

#### 第10周
1. 多玩家达康游戏
2. 增强for循环
3. 常用类
4. 包装类

#### 第11周
1. Collection 集合框架
2. ArrayList 的使用

#### 第12周
1. 泛型
2. Arrays，Collections
3. 异常

#### 第13周
1. Java IO

#### 第14周
1. Java ui
2. Java swing
3. JavaFx

#### 第15周
1. 仅实验

#### 第16周
1. 复习

