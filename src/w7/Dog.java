package w7;

public class Dog extends Canine {
	@Override
	public void makeNoise() {
		System.out.println("dog make noise");
	}
	@Override
	public void eat() {
		System.out.println("dog eat");
	}

}
