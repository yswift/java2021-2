package w7;

public class Wolf extends Canine {
	@Override
	public void makeNoise() {
		System.out.println("wolf make noise");
	}
	@Override
	public void eat() {
		System.out.println("wolf eat");
	}

}
