package w7;

public class WolfTest {
	public static void main(String[] args) {
//		Wolf w = new Wolf();
		Animal w = new Wolf();
		
		w.makeNoise();
		w.eat();
		w.sleep();
		w.roam();
		
		w = new Dog();
		w.makeNoise();
		w.eat();
		w.sleep();
		w.roam();
		
	}

}
