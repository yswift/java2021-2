package w7;

public class Sy10_2 {
	public static void main(String[] args) {
		Animal[] animals = new Animal[2];
		animals[0] = new Dog();
		animals[1] = new Wolf();
		
		for (int i=0; i<animals.length; i++) {
			animals[i].eat();
		}
	}

}
