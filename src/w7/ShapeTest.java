package w7;

public class ShapeTest {
	public static void main(String[] args) {
		Square s = new Square();
		Amoeba a = new Amoeba();
		
		s.rotate();
		s.playSound();
		
		a.rotate();
		a.playSound();
	}

}
