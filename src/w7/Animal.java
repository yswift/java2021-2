package w7;

public class Animal {
	String pciture;
	String food;
	int hunger;
	String boundaries;
	String location;
	
	public void makeNoise() {
		System.out.println("animal make noise");
	}
	
	public void eat() {
		System.out.println("animal eat");
	}
	
	public void sleep() {
		System.out.println("animal sleep");
	}
	
	public void roam() {
		System.out.println("animal roam");
	}

}
