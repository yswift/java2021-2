package dot;

public class RandomPlayer extends Player {

	@Override
	public String guess() {
		int r = (int)(Math.random()*7);
		System.out.println("randomPlayer guess: " + r);
		return String.valueOf(r);
	}

}
