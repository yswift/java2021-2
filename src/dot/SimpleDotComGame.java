package dot;
public class SimpleDotComGame {
	public static void main(String[] args) {
		int numOfGuesses = 0;//定义一个变量numofguesses初始值为0
		// GameHelper helper = new GameHelper();//为gamehelper类创建一个对象helper
		SimpleDotCom theDotCom = new SimpleDotCom();//为thedotcom类创建一个对象他thedotcom
		int randomNum = (int) (Math.random() * 5);//定义一个随机数变量产生0到5之间的整数
		int[] locations = { randomNum, randomNum + 1, randomNum + 2 };//定义一个locations数组三个元素为（随机数，随机数+1，随机数+2）
		theDotCom.setLocationCells(locations);//调用方法theDotCom中的setLocationCells，将locations数组作为形参赋给方法
		boolean isAlive = true;//定义一个boolean型变量初始化为true
		
		// 创建玩家
//		Player player = new HumanPlayer();
//		Player player = new RandomPlayer();
		Player player = new SequencePlayer();
		
		while (isAlive == true) {
			// 让玩家猜测
//			String guess = helper.getUserInput("enter a number");//若getUserInput方法没有抛异常则一直输出enter a number
			String guess = player.guess();
			
			String result = theDotCom.checkYourself(guess);//调用theDotCom中的checkYourself方法，此时guess为enter a number，如果没有抛异常
			// 告知玩家猜测结果
			player.setResult(result);
			
			numOfGuesses++;//记录循环次数
			if (result.equals("kill")) {//如果result恒等于kill则终止程序
				isAlive = false;
				System.out.println("You took " + numOfGuesses + " guesses");//输出你猜的次数
			} // close if
		} // close while
	}
}
