package dot;

public abstract class Player {
	protected String result;
	
	public abstract String guess();
	
	public void setResult(String result) {
		this.result = result;
	}

}
