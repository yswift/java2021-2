package dot;

public class SimpleDotCom {
	int[] locationCells;//定义一个数组locationCells，
	int numOfHits = 0;//定义一个变量numOfHits初始化为0

	public void setLocationCells(int[] locs) {//输入一个数组形参为locs，将形参赋值给变量数组locationCells，指向同一区域
		locationCells = locs;
	}
	public String checkYourself(String stringGuess) {//若没有抛异常，形参stringGuess为enter a number
		int guess = Integer.parseInt(stringGuess);//将stringGuess转化为整数的形式
		String result = "miss";//定义一个String变量为miss
//		for (int cell : locationCells) {//如果数组长度不为0则继续猜
//			if (guess == cell) {//若击中则继续循环，没有击中则输出result为miss
//				result = "hit";//result赋值为hit
//				numOfHits++;//击中的次数加一
//				break;//跳出循环
//			}
//		} // out of the loop
		for(int i = 0; i < locationCells.length; i++) {
			int cell = locationCells[i];
			if (guess == cell && guess != -1) {//若击中则继续循环，没有击中则输出result为miss
				result = "hit";//result赋值为hit
				numOfHits++;//击中的次数加一
				locationCells[i]= -1;
				break;//跳出循环
			}
		}
		if (numOfHits == locationCells.length) {//如果击中的个数恒等于数组的总个数则则输出kill，结束
			result = "kill";
		}
		System.out.println(result);//输出result，result的值有kill，hit，miss
		return result;//返回result
	}
}
