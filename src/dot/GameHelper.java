package dot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class GameHelper {
	Scanner input = new Scanner(System.in);
	
	public String getUserInput(String prompt) {
		System.out.print(prompt + " ");
		String inputLine = input.next();
		return inputLine;
	}
	
	//定义一个方法getUserInput，形参为String类型prompt
	//此时prompt==enter a number
	public String getUserInput_old(String prompt) {
		String inputLine = null;//定义inputLine初始化为null
		System.out.print(prompt + " ");//此时输出enter a number+一个空格
		
		try {//引用try，防止程序崩溃
			/*import java.io.BufferedReader;调用此包，用InputStreamReader这个中介把System.in这个字节流转换成
			字符流BufferedReader，设立缓冲区域，从而提高读取效率*/
			BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
			
			//import java.io.IOException;调用此包，readLine();主要作用是读取一行，,只有遇到回车(\r)或者换行符(\n)才会返回读取结果
			inputLine = is.readLine();
			
			if (inputLine.length() == 0)//如果读取的一行为null则返回null
				return null;
		} catch (IOException e) {//若try程序出错则执行catch (IOException e)，将异常输出
			System.out.println("IOException: " + e);
		}
		return inputLine;//若inputLine不为空则返回
	}
}
