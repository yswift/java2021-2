package w6;

public class DuckTest {
	public static void main(String[] args) {
		Duck d1 = new Duck(10);
//		d.setSize(10);
		System.out.println("d.size = " + d1.getSize());
		
		Duck d2 = new Duck();
		System.out.println("d2.size = " + d2.getSize());
	}

}
