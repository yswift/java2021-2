package w6;

public class PersonTest {
	public static void main(String[] args) {
		Person p = new Person("张三", 20, 1.8, 100);
		double bmi = p.calcBMI();
		System.out.println("bmi = " + bmi);

		String info = p.getBmiInfo();
		System.out.println(info);
	}

}
