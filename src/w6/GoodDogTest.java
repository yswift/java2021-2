package w6;

public class GoodDogTest {
	public static void main(String[] args) {
		GoodDog d1 = new GoodDog();
		// 赋值，要调用set
//		d1.size = 10;
		d1.setSize(10);
//		d1.name = "wangcai";
		d1.setName("wangcai");
		
		// 取值，调用get
//		System.out.println("d1.name = " + d1.name);
		System.out.println("d1.name = " + d1.getName());
		System.out.println("d1.size = " + d1.getSize());
		
		GoodDog d2 = new GoodDog();
		d2.setSize(-1);
		System.out.println("d2.size = " + d2.getSize());
	}

}
