package w6;

public class DogTest {
	public static void main(String[] args) {
		Dog d1 = new Dog();
		d1.size = 100;
		
		Dog d2 = new Dog();
		d2.size = 18;
		
		Dog d3 = new Dog();
		d3.size = 8;
		
		d1.bark();
		d2.bark();
		d3.bark();
	}

}
