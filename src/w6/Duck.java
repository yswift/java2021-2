package w6;

public class Duck {
	private int size;
	
	public Duck(int size) {
		this.size = size;
		System.out.println("Quack");
	}
	
	public Duck() {
		this(12);
//		this.size = 12;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	public int getSize() {
		return size;
	}

}
