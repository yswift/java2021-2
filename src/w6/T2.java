package w6;

public class T2 {
	static void f1(Dog d) {
		d.size = 100;
		System.out.println("d.size = " + d.size);
	}
	
	public static void main(String[] args) {
		Dog d1 = new Dog();
		d1.size = 10;
		System.out.println("d1.size = " + d1.size);
		f1(d1);
		System.out.println("d1.size = " + d1.size);
	}

}
