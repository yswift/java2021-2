package w6;

public class T4 {
	public static void main(String[] args) {
		// 方法调用
		Dog d1 = new Dog();
		d1.size = 100;
		d1.bark();
		
		d1.bark(3);
		d1.bark(3, 4);
		d1.bark("12");
		Long l1=10L;
		d1.bark(l1);
		short s1 = 10;
		d1.bark(s1);
	}
}
