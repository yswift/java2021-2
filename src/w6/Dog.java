package w6;

public class Dog {
	int size;
	String name;
	
	void bark() {
		if (size > 60) {
			System.out.println("wolf!");
		} else if (size > 14) {
			System.out.println("Ruff!");
		} else {
			System.out.println("Yip!");
		}
	}
	
	void bark(int numOfBarks) {
		while (numOfBarks > 0) {
			System.out.println("Ruff!");
			numOfBarks--;
		}
	}

}
