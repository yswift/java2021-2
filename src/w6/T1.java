package w6;

public class T1 {
	static void f1(int a) {
		a = 100;
		System.out.println("a = " + 100);
	}

	public static void main(String[] args) {
		int v = 10;
		System.out.println("v = " + v);
		f1(v);
		System.out.println("v = " + v);
	}
}
