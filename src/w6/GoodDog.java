package w6;

public class GoodDog {
	private int size;
	private String name;
	
	public void setSize(int s) {
		if (s < 1) {
			throw new IllegalArgumentException("size < 1, size=" + s);
		}
		size = s;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

}
