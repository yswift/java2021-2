package w10;

import java.time.LocalDateTime;

public class Lx1 {
	public static void main(String[] args) {
		LocalDateTime now = LocalDateTime.now();
		System.out.println("年: " + now.getYear());
		System.out.println("月: " + now.getMonth());
		System.out.println("日/月: " + now.getDayOfMonth());
		System.out.println("日/年: " + now.getDayOfYear());
		System.out.println("日/周: " + now.getDayOfWeek());
		
	}

}
