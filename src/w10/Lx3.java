package w10;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Lx3 {
	// 字符串转日期
	public static void main(String[] args) {
		String sd1 = "2021-11-5";
		String sd2 = "2021-11-05";
		String sd3 = "2021-1-05";
		String sd4 = "2021-01-05";
		
		DateTimeFormatter f1 =  DateTimeFormatter
				.ofPattern("yyyy-M-d");
		System.out.println(LocalDate.parse(sd1, f1));
		System.out.println(LocalDate.parse(sd2, f1));
		System.out.println(LocalDate.parse(sd3, f1));
		System.out.println(LocalDate.parse(sd4, f1));
	}

}
