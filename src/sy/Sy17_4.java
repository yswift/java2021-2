package sy;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sy17_4 {
	// 编写程序，从不同的编码文件（gbk.txt,utf8.txt）中正确的读取字符串，并输出。
	public static void main(String[] args) throws IOException {
		String fn = "E:\\utf8.txt";
		try (FileInputStream fis = new FileInputStream(fn);
		InputStreamReader isr = new InputStreamReader(fis, "UTF8");
		BufferedReader br = new BufferedReader(isr)) {
			for (;;) {
				String line = br.readLine();
				if (line == null) {
					break;
				}
				System.out.println(line);
			}
		}
	}

}
