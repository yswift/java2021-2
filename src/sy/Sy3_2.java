package sy;

import java.util.Scanner;

public class Sy3_2 {
	public static void main(String[] args) {
		System.out.println("输入身份证号:");
		Scanner in = new Scanner(System.in);
		String id = in.next();
		
		// 1）长度必须是18位字符；
		if (id.length() != 18) {
			System.out.println("验证失败，长度是:" + id.length());
			return;
		}
		
		// 2）1～17位由数字组成，最后一位是数字或X
		for (int i=0; i<17; i++) {
			char c = id.charAt(i);
			if (! Character.isDigit(c)) {
			//if (!(c >= '0' && c <= '9')) {
				System.out.println("第" + (i+1) + "位是" + c);
				return;
			}
		}
		char lastChar = id.charAt(17);
		if (!(Character.isDigit(lastChar) || lastChar=='x' || lastChar == 'X')) {
			System.out.println("第18位是" + lastChar);
		}
		
		// 3）计算并比较验证码
//		1、将前面的身份证号码17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 ；
		int[] xs = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
//		2、将这17位数字和系数相乘的结果相加；
		int sum = 0;
		for (int i=0; i<17; i++) {
			char c = id.charAt(i);
			sum += xs[i] * (c - '0');
		}
//		3、用加出来和除以11，看余数是多少；
		int ys = sum % 11;
//		4、余数只可能有0 1 2 3 4 5 6 7 8 9 10这11个数字。
//		     其分别对应的最后一位身份证的号码为1 0 X 9 8 7 6 5 4 3 2；
//		5、通过上面得知如果余数是2，余数所对应的最后一位身份证号是X，就会在身份证的第18位数字上出现罗马数字的X。
		String yzw = "10X98765432";
		char yzm = yzw.charAt(ys);
//		System.out.println("验证码：" + yzm);
		
		if (yzm != lastChar) {
			System.out.println("验证失败，最后一位应该是：" + yzm);
			return;
		}
		
		// 通过所有验证
		System.out.println("身份证号符合要求！");
	}

}
