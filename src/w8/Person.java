package w8;

public class Person {
	private String name;
	private int age;
	private double height;
	private double weight;
	
	public Person() {
		//super();
		
	}
	
	public Person(String name) {
		super();
		this.name = name;
	}
	
	public Person(String name, int age, double height, double weight) {
		//this.name = name;
		this(name);
		this.age = age;
		this.height = height;
		this.weight = weight;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	public void setAge(int age) {
		if (age < 1 || age > 120) {
			throw new IllegalArgumentException("age = " + age);
		}
		this.age = age;
	}
	public int getAge() {
		return age;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	public double getHeight() {
		return height;
	}
	
	public double calcBMI() {
		double bmi = weight / (height*height);
		return bmi;
	}
	
	public String getBmiInfo() {
		double bmi = calcBMI();
//		过轻：低于18.5
//		正常：18.5-24.99
//		过重：25-28
//		肥胖：28-32
//		非常肥胖, 高于32
		if (bmi < 18.5) {
			return "过轻";
		} else if (bmi < 25) {
			return "正常";
		} else if (bmi < 28) {
			return "过重";
		} else if (bmi < 32) {
			return "肥胖";
		} else {
			return "非常肥胖";
		}
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", height=" + height + ", weight=" + weight + "]";
	}

	
}
