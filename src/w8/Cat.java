package w8;

public class Cat extends Feline implements Pet {
	@Override
	public void beFriendly() {
		System.out.println("cat be friendly");
	}

	@Override
	public void play() {
		System.out.println("cat play");
	}

	@Override
	public void makeNoise() {
		System.out.println("cat make noise");
	}

	@Override
	public void eat() {
		System.out.println("cat eat");
	}

}
