package w8;

public class RoboDog extends Robot implements Pet {
	@Override
	public void beFriendly() {
		System.out.println("robo dog be friendly");
	}

	@Override
	public void play() {
		System.out.println("robo dog play");
	}

}
