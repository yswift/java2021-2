package w8;

public class TestPet {
	public static void main(String[] args) {
		Cat c = new Cat();
		RoboDog rd = new RoboDog();
		
		Pet p = c;
		System.out.println("p = c");
		p.beFriendly();
		p.play();
		
		p = rd;
		System.out.println("\np = rd");
		p.beFriendly();
		p.play();
		
	}

}
