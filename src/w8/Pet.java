package w8;

public interface Pet {
	public abstract void beFriendly();
	
	// 接口中的方法默认是 public abstract
	void play();

}
