package w8;

public abstract class Animal {
	String pciture;
	String food;
	int hunger;
	String boundaries;
	String location;
	
	public Animal() {
		System.out.println("new animal");
	}

//	 方法声明为抽象的，
//	 要求具体的子类一定要覆盖/重写/override这个方法
	public abstract void makeNoise();
	
	public abstract void eat();
	
	public void sleep() {
		System.out.println("animal sleep");
	}
	
	public void roam() {
		System.out.println("animal roam");
	}

}
