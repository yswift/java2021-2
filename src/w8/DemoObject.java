package w8;

public class DemoObject {
	public static void main(String[] args) {
		Object o1 = new Object();
		Object o2 = new Object();
		
		System.out.println("o1.equals(o2) = " + o1.equals(o2));
		System.out.println();
		
		System.out.println("o1.hashCode = " + o1.hashCode());
		System.out.println("o2.hashCode = " + o2.hashCode());
		System.out.println();
		
		System.out.println("o1.toString = " + o1);
		System.out.println("o1.toString = " + o1.toString());
		System.out.println("o2.toString = " + o2);
		System.out.println();
		
		System.out.println("o1.getClass = " + o1.getClass());
		System.out.println("o2.getClass = " + o2.getClass());
	}

}
