package w8;

public abstract class Feline extends Animal {
	public Feline() {
		System.out.println("new feline");
	}
	
	@Override
	public void roam() {
		System.out.println("feline roam");
	}
}
