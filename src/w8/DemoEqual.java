package w8;

public class DemoEqual {
	public static void main(String[] args) {
		String s1 = "abc";
		String s2 = "abc123".substring(0, 3);
		String s3 = s1;
		
		// == 比较是否同一个对象
		System.out.println("s1 == s2 ? " + (s1 == s2));
		System.out.println("s1 == s3 ? " + (s1 == s3));
		// equlas 逻辑相等
		System.out.println("s1.equals(s2)? " + s1.equals(s2));
	}

}
