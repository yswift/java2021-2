package w8;

public class TestCat {
	public static void main(String[] args) {
		Cat c = new Cat();
		
		c.eat();
		c.makeNoise();
		c.roam();
		c.sleep();
		c.beFriendly();
		c.play();
		
		Animal a = c;
		a.eat();
		a.makeNoise();
		a.roam();
		a.sleep();
//		a.beFriendly();
//		a.play();

		Pet p = c;
//		p.eat();
//		p.makeNoise();
//		p.roam();
//		p.sleep();
		p.beFriendly();
		p.play();
		
	}
}
