package w8;

public class Tiger extends Feline {
	public Tiger() {
		super();
		System.out.println("new tiger");
	}
	@Override
	public void makeNoise() {
		System.out.println("tiager make noise");
	}

	@Override
	public void eat() {
		System.out.println("tiger eat");
	}
	
	@Override
	public void roam() {
		super.roam();
		System.out.println("tiger roam");
	}
	
	@Override
	public String toString() {
		return "老虎";
	}
}
