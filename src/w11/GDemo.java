package w11;

import java.util.ArrayList;

import w12.Person;

public class GDemo {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("张飞");
		list.add(100);
		list.add(new Person("刘备"));
		
		Object o1 = list.get(0);
		Person p = (Person) list.get(2);
		
		for (Object s : list) {
			String v = (String)s;
			System.out.println(v);
		}
	}

}
