package w11;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo2 {
	public static void main(String[] args) {
//		产生5个10~20之间的随机数，并保存到ArrayList中
		ArrayList<Integer> list = new ArrayList<>();
		
		for (int i=0; i<5; i++) {
			int r = (int)(Math.random()*10+10);
			// 自动装箱
			list.add(r);
		}
		System.out.println(list);
//		求上述ArrayList中的平均值
		double sum = 0;
		// 自动拆箱
		for (int v : list) {
			sum += v;
		}
		double avg = sum / list.size();
		System.out.println("avg = " + avg);

		ArrayList<Integer> list2 = new ArrayList(list);
//		删除上述ArrayList中小于平均值的数
		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			int v = it.next();
			if (v < avg) {
				it.remove();
			}
		}
		System.out.println("删除上述ArrayList中小于平均值的数");
		System.out.println(list);

		// 下面的删除会出问题
		for (int i=0; i<list2.size(); i++) {
			if (list2.get(i) < avg) {
				list2.remove(i);
			}
		}
		System.out.println(list2);
	}

}
