package w11;

import java.util.ArrayList;

import w12.Person;

public class ArrayListDemo3 {
	public static void main(String[] args) {
//		创建实验5中的3个Person对象，把他们放入ArrayList中
		ArrayList<Person> list = new ArrayList<>();
		list.add(new Person("刘备", 30, 1.7, 60));
		Person gy = new Person("关羽", 28, 1.9, 90);
		list.add(gy);
		list.add(new Person("张飞", 27, 2.0, 100));
		
		System.out.println(list);
		
//		求上述ArrayList中的平均升高。
		double sum = 0;
		for (Person p : list) {
			sum += p.getHeight();
		}
		double avg = sum / list.size();
		System.out.println("avg = " + avg);

//		输出大于平均升高的人的名字。
		for (Person p : list) {
			if (p.getHeight() > avg) {
				System.out.println(p.getName());
			}
		}

	}
}
