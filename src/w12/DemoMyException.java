package w12;

public class DemoMyException {
	public static void main(String[] args) {
		Person p = new Person("刘备");
		try {
			p.setAge(1);
			System.out.println(p);
			System.out.println("try finish");
		} catch (MyException e) {
			e.printStackTrace();
		} finally {
			System.out.println("finally end");
		}
		System.out.println("main finish");
	}

}
