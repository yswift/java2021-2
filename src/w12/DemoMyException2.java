package w12;

public class DemoMyException2 {
	public static void main(String[] args) throws MyException{
		// 2、不处理，再次表示本方法有可能失败

		Person p = new Person("刘备");
		p.setAge(1);
		System.out.println(p);
		System.out.println("main finish");
	}
}
