package w12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapDemo {
	// 按名字找人
	static Person findByName(ArrayList<Person> list, String name) {
		for (Person p : list) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
	
	// 按名字找人
	static Person findByName(Map<String, Person> name2Person, String name) {
		return name2Person.get(name);
	}
	
	public static void main(String[] args) {
		ArrayList<Person> list = new ArrayList<>();
		list.add(new Person("刘备", 30, 1.7, 60));
		Person gy = new Person("关羽", 28, 1.9, 90);
		list.add(gy);
		list.add(new Person("张飞", 27, 2.0, 100));
		
		Person gy2 = findByName(list, "关羽");
		System.out.println(gy2);
		Person zy = findByName(list, "赵云");
		System.out.println(zy);
		
		Map<String, Person> name2Person = new HashMap<>();
		name2Person.put("刘备", new Person("刘备", 30, 1.7, 60));
		name2Person.put("关羽", new Person("关羽", 28, 1.9, 90));
		name2Person.put("张飞", new Person("张飞", 27, 2.0, 100));
	}

}
