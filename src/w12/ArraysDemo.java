package w12;

import java.util.Arrays;

public class ArraysDemo {
	public static void main(String[] args) {
		int[] a = new int[10];
		for (int i=0; i<a.length; i++) {
			a[i] = (int)(Math.random()*100);
		}
		System.out.println(Arrays.toString(a));
		
		// 排序
		System.out.println("排序");
		Arrays.sort(a);
		System.out.println(Arrays.toString(a));
	}

}
