package w12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Array2ListDemo {
	static void array2List() {
		Integer[] a = new Integer[10];
		for (int i=0; i<a.length; i++) {
			a[i] = (int)(Math.random()*100);
		}
		// 转List
		List<Integer> list = Arrays.asList(a);
		// list.add(100);
		// 方法1
		List<Integer> nlist1 = new ArrayList<>(list);
		// 方法2
		List<Integer> nlist2 = new ArrayList<>(list.size());
		Collections.addAll(nlist2, a);
		System.out.println(list);
	}
	
	static void list2Array() {
		ArrayList<Integer> list = new ArrayList<>();
		for (int i=0; i<10; i++) {
			list.add((int)(Math.random()*100));
		}
		Integer[] a = list.toArray(new Integer[list.size()]);
		
		Integer[] a1 = list.toArray(new Integer[0]);
		System.out.println(a1.length);
	}

	public static void main(String[] args) {
//		array2List();
		list2Array();
	}
}
