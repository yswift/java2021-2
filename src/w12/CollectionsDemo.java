package w12;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionsDemo {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		for (int i=0; i<10; i++) {
			list.add((int)(Math.random()*100));
		}
		System.out.println(list);
		
		System.out.println("最大值");
		Integer max = Collections.max(list);
		System.out.println("max = " + max);
		
		System.out.println("\n排序");
		Collections.sort(list);
		System.out.println(list);

		System.out.println("\n乱序");
		Collections.shuffle(list);
		System.out.println(list);

	}
}
