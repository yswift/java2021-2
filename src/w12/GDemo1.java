package w12;

import java.util.ArrayList;

import w8.*;

public class GDemo1 {
	// 对元素求和
	static double sum(ArrayList<? extends Number> list) {
		double sum = 0;
		for (Number n : list) {
			sum += n.doubleValue();
		}
		return sum;
	}
	
	static void f1() {
		ArrayList<Number> list = new ArrayList<>();
		list.add(Long.valueOf(100L));
		list.add(Double.valueOf(10.5));
		
		ArrayList<Integer> listInt = new ArrayList<>();
		ArrayList<? extends Number> listNum = listInt;
		// <? extends Number>集合 不能添加元素
		listNum.add(Long.valueOf(100L));
		listNum.add(Double.valueOf(10.5));
	}
	
	static void f2() {
		ArrayList<Animal> listAnimal = new ArrayList<>();
		listAnimal.add(new Tiger());
		
		ArrayList<? super Feline> listFeline = listAnimal;
		Feline c1 = listFeline.get(0);
		listFeline.add(new Lion());
		
	}
	
	
	public static void main(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<>();
		list1.add(10);
		list1.add(20);
		list1.add(30);
		System.out.println(list1);
		
		double sum = sum(list1);
		System.out.println("sum1 = " + sum);
		
		ArrayList<Double> list2 = new ArrayList<>();
		list2.add(100.0);
		list2.add(200.0);
		list2.add(300.0);
		System.out.println(list2);
		double sum2 = sum(list2);
		System.out.println("sum2 = " + sum2);
	}

}
