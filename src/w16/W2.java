package w16;

public class W2 {
	public static void main(String[] args) {
		S s = new S("abc", "123");
		System.out.println(s);
	}
}

class F {
	protected String name;
}

class S extends F {
	public String name;
	
	S(String fname, String sname) {
		super.name = fname;
		this.name = sname;
	}

	@Override
	public String toString() {
		return "S [f.name = " + super.name + ", name=" + name + "]";
	}
}