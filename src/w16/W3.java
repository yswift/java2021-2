package w16;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class W3 {
	public static void main(String[] args) throws IOException {
		// 分别把 1， 和'1'写入文件
		try (FileOutputStream fos = new FileOutputStream("d:\\w3-1.txt")) {
			fos.write(1);
		}
		try (FileOutputStream fos = new FileOutputStream("d:\\w3-2.txt");
				OutputStreamWriter fw = new OutputStreamWriter(fos)) {
			fw.write('1');
		}
	}
}
