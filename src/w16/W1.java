package w16;

import java.io.FileInputStream;
import java.io.IOException;

public class W1 {
	public static void main(String[] args) {
		try {
			FileInputStream fis = new FileInputStream("d:\\a1.txt");
			int d = fis.read();
			fis.close();
			System.out.println("读取完成");
		} catch (IOException e) {
			System.out.println("从文件读取数据失败，" + e.getMessage());
		} finally {
			System.out.println("try-catch-finally 完成");
		}
	}
}
