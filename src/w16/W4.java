package w16;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class W4 {
	static char c = '我';
	
	// 用字节流，按bgk 编码，把字符c写入文件
	static void w1() throws IOException {
		byte[] arr = { (byte)0xce, (byte)0xd2 };
		try (FileOutputStream fos = new FileOutputStream("d:\\w4-1.txt")) {
			fos.write(arr);
		}
	}
	// 用字符流，按参数给定的编码，把字符c写入文件
	static void w2(String charset) throws IOException {
		try (FileOutputStream fos = new FileOutputStream("d:\\w4-" + charset + ".txt");
				OutputStreamWriter fw = new OutputStreamWriter(fos, charset)) {
			fw.write('我');
		}
	}
	
	public static void main(String[] args) throws IOException {
		// 把字符“我”写入文件
		w1();
		w2("GBK");
		w2("UTF8");
		w2("UTF16");
	}

}
