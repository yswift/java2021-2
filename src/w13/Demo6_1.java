package w13;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo6_1 {
	public static void main(String[] args) throws IOException {
		FileInputStream fin = new FileInputStream("e:\\123.txt");
		FileOutputStream fout = new FileOutputStream("e:\\456.txt");
		int length = fin.read(); // 读一个字节到内存length
		while (length != -1) {
			fout.write(length); // 将内存中的一个字节length写入到文件
			length = fin.read();
		}
		System.out.println("复制完成");
		fout.close();
		fin.close();
	}
}
