package w13;

public class Demo11 {
	public static void main(String[] args) throws Exception {
		FileInputStream in = new FileInputStream("d:\\456.txt");
		InputStreamReader ins = new InputStreamReader(in);
		BufferedReader bufferedReader = new BufferedReader(ins);
		FileOutputStream out = new FileOutputStream("d:\\des.txt");
		OutputStreamWriter outs = new OutputStreamWriter(out);
		BufferedWriter bufferedWriter = new BufferedWriter(outs);
		String str;
		while ((str = bufferedReader.readLine()) != null) {
			bufferedWriter.write(str);
			bufferedWriter.newLine();
		}
		bufferedReader.close();
		bufferedWriter.close();
	}
}
