package w13;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo6_2 {
	public static void main(String[] args) throws IOException {
		FileInputStream fin = new FileInputStream("e:\\123.txt");
		FileOutputStream fout = new FileOutputStream("e:\\456.txt");
		byte[] bytes = new byte[1024];
		for (;;) {
			int length = fin.read(bytes);
			if (length == -1) break;
			fout.write(bytes, 0, length);
		}
//		int length = fin.read(bytes); // 读一个字节到内存length
//		while (length != -1) {
//			fout.write(bytes, 0, length); // 将内存中的一个字节length写入到文件
//			length = fin.read(bytes);
//		}
		System.out.println("复制完成");
		fout.close();
		fin.close();
	}
}
