package w13;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Demo8_2 {
	public static void main(String[] args) throws IOException {
		try(FileOutputStream fos = new FileOutputStream("e:\\456.txt");
			OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF16")) {
			writer.write("字符流输出");
			writer.close();
			fos.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		}		
	}
}
