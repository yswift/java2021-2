package w13;

import java.io.FileOutputStream;
import java.io.IOException;

public class Demo4_1 {
	public static void main(String[] args) throws IOException {
		FileOutputStream fout = new FileOutputStream("e:\\123.txt");
//根据文件路径，创建一个文件输出流对象
		fout.write('a');
		// 采用fout对象，写入字符‘a’到文件中。
		// 如果是写入字符串，建议都使用字符流
		fout.write('文');
//	System.out.println("写入成功！");
	}

}
