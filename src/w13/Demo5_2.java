package w13;

import java.io.FileInputStream;
import java.io.IOException;

public class Demo5_2 {
	public static void main(String[] args) throws IOException {
		FileInputStream fs = new FileInputStream("e:\\123.txt");
		byte[] bytes = new byte[1024];// 创建字节数组
		int length = fs.read(bytes);
//        一次性读取数组大小的数据，返回读取数据的长度
		while (length != -1) {
			// 如果length！=-1，说明文件数据没有读完
			System.out.println(new String(bytes, 0, length));
			// 将每次读取的字节数组重构成字符串打印输出
			length = fs.read(bytes);
			// 继续读取数据。
		}
	}

}
