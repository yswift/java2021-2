package w13;

public class Demo10 {
	public static void main(String[] args) throws Exception {
		FileReader fileReader = new FileReader("d:\\456.txt");
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		FileWriter fileWriter = new FileWriter("d:\\des.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		String str;
		while ((str = bufferedReader.readLine()) != null) {
			bufferedWriter.write(str);
			bufferedWriter.newLine();
			// 写入换行符
		}
		bufferedReader.close();
		bufferedWriter.close();
	}
}