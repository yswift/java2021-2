package w13;

import java.io.File;

public class Demo3 {
	public static void main(String[] args) {
		File file = new File("e:\\Demo1");
		if (file.isFile()) {
			file.delete();
		} else {
			deleteDir(file);
		}
	}

	public static void deleteDir(File file) {
		if (file.exists()) {
			File[] files = file.listFiles();
			for (File file1 : files) {
				if (file1.isDirectory()) {
					deleteDir(file1);
				} else {
					file1.delete();
				}
			}
		}
		file.delete();
	}
}
