package w13;

import java.io.File;

public class Demo1 {
	public static void main(String[] args) {
		String path = "e:\\Demo1.txt";
		File file = new File(path); // 采用构造方法，创建一个文件对象
		// 1.判断方法
		System.out.println(file.exists() == true ? "存在" : "不存在");
		System.out.println(file.isDirectory() == true ? "是文件目录" : "不是文件目录");
		System.out.println(file.isFile() == true ? "是文件" : "不是文件");
		System.out.println(file.canRead() == true ? "可读" : "不可读");
		System.out.println(file.canWrite() == true ? "可写" : "不可写");
		System.out.println(file.canExecute() == true ? "可执行" : "不可执行");
		// 2.获得方法
		System.out.println("文件路径是：" + file.getPath());
		System.out.println("文件绝对路径是：" + file.getAbsolutePath());
		System.out.println("父文件路径是：" + file.getParent());
		System.out.println("文件名是：" + file.getName());
	}
}
