package w13;

public class Demo9 {
	/** * 一个字符一个字符的读取 * @throws Exception */
	public void readOne() throws Exception {
		FileReader fileReader = new FileReader("e:\\456.txt");
		int read = fileReader.read();
		while (read != -1) {
			System.out.print((char) read);
			read = fileReader.read();
		}
		fileReader.close();
	}

	/**
	 * 一次性读入到字符数组中
	 * 
	 * @throws Exception
	 */
	public void readBuff() throws Exception {
		FileReader fileReader = new FileReader("e:\\456.txt");
		char[] chars = new char[100];
		int len = fileReader.read(chars);
		while (len != -1) {
			System.out.print(new String(chars, 0, len));
			len = fileReader.read(chars);
		}
		fileReader.close();
	}
}
