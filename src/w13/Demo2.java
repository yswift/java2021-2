package w13;

import java.io.File;

public class Demo2 {
	public static void main(String[] args) {
		File file = new File("e:\\eclipse");
		if (file.isFile()) {
			System.out.println(file.getName());
		} else {
			listName(file);
		}
	}

	/** *定义一个列出文件列表的方法，该方法时一个递归方法，终止条件是当文件中没有目录，全为文件 * 打印时停止递归 * @param file */
	public static void listName(File file) {
		if (file.isFile()) {
			System.out.println("--" + file.getName());
		} else {
			System.out.println("||" + file.getName());
			File[] files = file.listFiles();
			for (File file1 : files) {
				listName(file1);
			}
		}
	}
}
