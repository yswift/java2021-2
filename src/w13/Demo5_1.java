package w13;

import java.io.FileInputStream;
import java.io.IOException;

public class Demo5_1 {
	public static void main(String[] args) throws IOException {
		FileInputStream fs = new FileInputStream("e:\\123.txt");
		// 根据文件路径，创建一个文件输入流对象
		int content = fs.read(); // 读取一个字节的二进制流，赋值给content变量
		while (content != -1) {
			// 如果数据读完，返回值为-1.
			System.out.print((char) content);
			content = fs.read(); // 继续读取数据
		}
	}

}
