package w13;

public class Demo4_3 {
	public static void main(String[] args) throws IOException {
		FileOutputStream fout = new FileOutputStream("e:\\123.txt", true);
		// 设置为true，就是追加写入
		byte[] bytes = "\r\n sdfhksdjfhksdfhjksdfhgjksdfsdfkhskd".getBytes();
		// "\r\n" 追加时换行
		// 将字符串转化为字节数组。
		fout.write(bytes);
		// 一次性写入字节数组
		System.out.println("写入成功!");
		// 关闭资源
		fout.close();
	}

}
