package w2;

import java.util.Scanner;

public class Zy2_1 {
	// 写函数（方法）判断是否是闰年
	// 是闰年，返回true
	// 不是闰年，返回false
	static boolean isLeapYear(int year) {
		if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
			return true;
		} else {
			return false;
		}
	}
	
	static boolean isLeapYear1(int year) {
		return (year % 400 == 0 
				|| (year % 4 == 0 && year % 100 != 0));
	}

	// 在main方法中调用判断闰年的方法
	public static void main(String[] args) {
		// 提示输入年份
		System.out.println("输入年份：");
		// 输入年
		Scanner in = new Scanner(System.in);
		int year = in.nextInt();
		// if (isLeapYear(year) == true) {
		if (isLeapYear(year)) {
			System.out.println(year + "是闰年");
		} else {
			System.out.println(year + "不是闰年");
		}
	}

}
