package w2;

import java.util.Scanner;

public class Zy3_1 {
	// 写方法，判断方法参数n, 是否是素数，
	// 是素数返回true，不是返回false
	static boolean isPrime(int n) {
		boolean isPrime = true;
		for (int i=2; i<=n/2; i++) {
			if (n % i == 0) {
				isPrime = false;
				break;
			}
		}
		return isPrime;
	}
	
	
	// 写main方法，调用判断素数的方法，判断输入的数是否是素数
	public static void main(String[] args) {
		// 1）输入一个数，
		System.out.println("输入整数:");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		// 2) 调用方法判断，并输出
		if (isPrime(n)) {
			System.out.println(n + "是素数");
		} else {
			System.out.println(n + "不是素数");
		}
	}

}
