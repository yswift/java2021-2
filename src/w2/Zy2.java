package w2;

import java.util.Scanner;

public class Zy2 {
	public static void main(String[] args) {
		// 提示输入年份
		System.out.println("输入年份：");
		// 输入年
		Scanner in = new Scanner(System.in);
		int year = in.nextInt();
		// 判断是否闰年，并输出
		if (year % 400 == 0 
				|| (year%4==0 && year%100!=0)) {
			System.out.println(year + "是闰年");
		} else {
			System.out.println(year + "不是闰年");
		}
		// 判断方法：能够被400整除，或者能够被4整除且不能被100整除。

	}

}
