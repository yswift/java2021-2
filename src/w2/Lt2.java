package w2;

public class Lt2 {
	// 求1..n的和
	static int sum(int n) {
		int sum = 0;
		for (int i=1; i<=n; i++) {
			sum += i;
		}
		return sum;
	}
	
	public static void main(String[] args) {
		int s100 = sum(100);
		int s200 = sum(200);
		int s = sum(50);
		sum(40); // 忽略返回值
		System.out.println("sum(1..100) = " + s100);
		System.out.println("sum(1..200) = " + s200);
		System.out.println("sum(1..300) = " + sum(300));
	}
}
