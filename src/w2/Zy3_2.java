package w2;

public class Zy3_2 {
	static boolean isPrime(int n) {
		for (int i=2; i<=n/2; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		// 验证哥德巴赫猜想，大于6的偶数可以分解成两个素数的和
		
		// 验证 6-100
		for (int n=6; n<=100; n+=2) {
			// 验证 n
			for (int i=2; i<=n/2; i++) {
				// 先 2,(n-2)是否是素数
				// 再验证 3，(n-3)是否是素数
				// 直到 n/2, n/2是否是素数
				if (isPrime(i) && isPrime(n-i)) {
					System.out.println(i+" + "+(n-i) + " = " + n);
//					break;
				}
			}
		}
	}

}
