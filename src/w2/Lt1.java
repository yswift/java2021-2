package w2;

import java.util.Scanner;

public class Lt1 {
	public static void main(String[] args) {
		// 输入三个整数x,y,z，请把这三个数由小到大输出
		Scanner in = new Scanner(System.in);
		System.out.println("输入三个数：");
		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		
		if (x > y) {
			int t = x;
			x = y; 
			y = t;
		}
		if (x > z) {
			int t = x;
			x = z;
			z = t;
		}
		if (y > z) {
			int t = y;
			y = z;
			z = t;
		}
		
		System.out.println(x + " < " + y + " < " + z);
	}

}
