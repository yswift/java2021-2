package w2;

import java.util.Scanner;

public class Zy3 {
	public static void main(String[] args) {
		// 1）输入一个数，
		System.out.println("输入整数:");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		// 2）判断这个数是否是素数，并输出
		// 判断方法:
		// 用2..(n/2)去除这个数，若能整除，不是素数
		// 若都不能整除，是素数
		boolean isPrime = true;
		for (int i=2; i<=n/2; i++) {
			if (n % i == 0) {
				isPrime = false;
				break;
			}
		}
		// 3) 输出
		if (isPrime) {
			System.out.println(n + "是素数");
		} else {
			System.out.println(n + "不是素数");
		}
	}

}
