package w4;

public class Dog {
	// 属性/实例变量
	int size;
	String breed;
	String name;

	// 行为/方法
	void bark() {
		System.out.println(name + " Ruff! Ruff!");
	}
}
