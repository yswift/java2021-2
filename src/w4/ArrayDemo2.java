package w4;

import java.util.Random;

public class ArrayDemo2 {
//	写一个方法：用0-100的随机数填充这个数组
	static void init(int[] a) {
		Random r = new Random();
		for (int i=0; i<a.length; i++) {
			a[i] = r.nextInt(100);
		}
	}
//	写一个方法：求这个数组中的最大值
	static int max(int[] a) {
//		int max = 0;
//		int max = a[0];
		int max = Integer.MIN_VALUE;
		for (int i=0; i<a.length; i++) {
			if (a[i]>max) {
				max = a[i];
			}
		}
		return max;
	}
//	写一个方法：求这个数组中的最小值
	static int min(int[] a) {
		int min = Integer.MAX_VALUE;
		for (int i=0; i<a.length; i++) {
			if (min > a[i]) {
				min = a[i];
			}
		}
		return min;
	}
	
//	写一个方法：求这个数组的平均值
	static double avg(int[] a) {
		double sum = 0;
		for (int i=0; i<a.length; i++) {
			sum += a[i];
		}
		return sum / a.length;
	}
	// 查找值，返回值的索引位置，找不到返回-1
	static int indexOf(int[] a, int v) {
		for (int i=0; i<a.length; i++) {
			if (v == a[i]) {
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		// 定义一个10个元素的整形的数组
		int[] a = new int[10];
		init(a);
		for (int i=0; i<a.length; i++) {
			System.out.print(a[i] + ",");
		}
		System.out.println();
		
		int maxValue = max(a);
		System.out.println("max value = " + maxValue);
		
		double avgValue = avg(a);
		System.out.println("average = " + avgValue);
		
		int idx = indexOf(a, maxValue);
		System.out.println("idx = " + idx);
		
	}


}
