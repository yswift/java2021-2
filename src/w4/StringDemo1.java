package w4;

public class StringDemo1 {
	public static void main(String[] args) {
		String message = "Welcome " + "to " + "Java";
		System.out.println("message = " + message);
		
		// 注意计算的优先级
		int a = 10;
		int b = 20;
		System.out.println("a + b = " + a + b);	
		System.out.println("a + b = " + (a + b));
	}

}
