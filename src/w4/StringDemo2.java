package w4;

public class StringDemo2 {
	public static void main(String[] args) {
		String str = "hello world";
		//  1)	输出字符串str的值的大小写版本；
		String ustr = str.toUpperCase();
		String lstr = str.toLowerCase();
		System.out.println("大写:" + ustr);
		System.out.println("小写:" + lstr);
		//	2)	求str的长度；  
		int len = str.length();
		System.out.println("长度: " + len);
		//	3)	判断str在忽略大小写的前提下是否与其大写版本相等；
		boolean e1 = str.equalsIgnoreCase(ustr);
		System.out.println("忽略大小写比较：" + e1);
		
		//	4)	判断str在不忽略大小写的前提下是否与其大写版本相等；
		boolean e2 = str.equals(ustr);
		System.out.println("不忽略大小写比较：" + e2);
		
		//	5)	求出索引位置为7的字符；
		// 注意：开始位置是0
		char c7 = str.charAt(7);
		System.out.println("索引位置为7的字符:" + c7);
		//	6)	求出第一个出现字符“l”的索引；
		int idxL = str.indexOf('l');
		System.out.println("第一个出现字符“l”的索引:" + idxL);
		//	7)	求出最后一次出现“l”的索引；  
		int idxlL = str.lastIndexOf('l');
		System.out.println("最后一个出现字符“l”的索引:" + idxlL);
		//	8)	求出子串“good”在str中出现的索引位置；  
		int idx2 = str.indexOf("good");
		System.out.println("“good”在str中出现的索引位置:" + idx2);
		
		//	9)	输出str中从位置5到位置9的子串；  
		String sub1 = str.substring(5, 9);
		System.out.println("位置5到位置9的子串:[" + sub1 + "]");
		//	10)	将str中的所有“l”替换成“x”后输出；  
		String str2 = str.replaceAll("l", "x");
		System.out.println("所有“l”替换成“x”:" + str2);
		//	11)	将字符串str转换为字符数组，并将字符数组中的每个字符打印出来。
		char[] array1 = str.toCharArray();
		for (int i=0; i<array1.length; i++) {
			System.out.println(array1[i]);
		}
	}

}
