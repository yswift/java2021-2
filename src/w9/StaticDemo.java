package w9;

public class StaticDemo {
	static int sv = 20;
	
	int v = 10;
	
	void f1() {
		System.out.println("f1(), v = " + v);
		System.out.println("sv = " + sv);
	}
	
	static void f2() {
		// 不能直接访问变量：v
		System.out.println("f2(), sv = " + sv);
	}
	
	public static void main(String[] args) {
		// 通过创建对象来调用非静态的方法
		StaticDemo sd = new StaticDemo();
		sd.f1();
		f2();
	}

}
