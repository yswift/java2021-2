package w9;

public class Outer {
	int ov = 10;
	
	class Inner1 {
		void f1() {
			System.out.println("ov = " + ov);
		}
	}
	
	static class Inner2 {
		void f1() {
			System.out.println("ov = " + ov);
		}		
	}

	
}
