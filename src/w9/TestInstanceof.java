package w9;

import w8.*;

public class TestInstanceof {
	public static void main(String[] args) {
		Cat c = new Cat();
		
		// 向上转型
		Animal a = c;
		a.eat();
		
		// 向下
//		a = new Tiger();
		if (a instanceof Cat) {
			Cat c2 = (Cat) a;
			System.out.println("转型成功");
		} else {
			System.out.println("转型失败");
		}
		
		System.out.println("\ninstanceof demo");
		System.out.println("a is Cat?" + (a instanceof Cat));
		System.out.println("a is Feline?" + (a instanceof Feline));
		System.out.println("a is Pet?" + (a instanceof Pet));
		System.out.println("a is Object?" + (a instanceof Object));
		System.out.println("a is Tiger?" + (a instanceof Tiger));
	}

}
