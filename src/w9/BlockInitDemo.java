package w9;

// 代码块执行次序
public class BlockInitDemo {
	static int no = 0;
	
	static Var sv = new Var(++no + ")静态变量赋值");
	
	Var v = new Var(++no + ")实例变量赋值");
	
	static {
		System.out.println(++no + ")执行静态代码块");
	}
	
	{
		System.out.println(++no + ")执行构造代码块");
	}
	
	public BlockInitDemo() {
		System.out.println(++no + ")执行构造函数");
	}
}
