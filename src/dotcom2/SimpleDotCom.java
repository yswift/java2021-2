package dotcom2;

import java.util.ArrayList;

public class SimpleDotCom {
	ArrayList<String> locationCells;
//	int numOfHits = 0;

	public void setLocationCells(ArrayList<String> locs) {
		locationCells = locs;
	}

	public String checkYourself(String stringGuess) {
//		int guess = Integer.parseInt(stringGuess);
		String result = "miss";

//		for (int cell : locationCells) {
//			if (guess == cell) {
//				result = "hit";
//				numOfHits++;
//				break;
//			}
//		} // out of the loop
		if (locationCells.remove(stringGuess)) {
			result = "hit";
		}
		
//		if (numOfHits == locationCells.length) {
//			result = "kill";
//		}
		if (locationCells.isEmpty()) {
			result = "kill";
		}
		System.out.println(result);
		return result;
	}
}
